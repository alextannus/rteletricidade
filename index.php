<!doctype html>
<html lang="pt-br">
	<head>
		<?php require_once("header.html"); ?>
	</head>

	<body>
		<?php require_once("barranavegacao.html"); ?>
		

		<div class="container">
			<?php require_once("slider.html"); ?>
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header">
						<h1>Serviços</h1>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12" style="margin-bottom:50px;">
					Supervisão e controle de sistemas elétricos em subestações e centros de operação, privilegiando a tomada de decisões pelos operadores.
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5 jumbotron">
					PROJETOS: <br/><br/>
					<ul>
						<li>Projetos elétricos industriais</li>
						<li>Redes de Computadores</li>
						<LI>Projetos com o SAGE - Sistema Aberto de Gerenciamento de Energia, do CEPEL.</LI>
					</ul>
				</div>

				<div class="col-sm-5 jumbotron">
					SAGE para: <br/><br/>
					<ul>
						<li>Centros de Operação</li>
						<li>Subestações</li>
						<li>Usinas</li>
						<li>Plantas industriais</li>
					</ul>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">
					<div class="page-header">
						<h1>Clientes</h1>
					</div>
				</div>
			</div>

			<div class="row" id="client-slide">
				<div class="col-sm-12">
					<?php require_once("standard.php"); ?>
				</div>
			</div>
			

		</div>
		<?php require_once("footer.html"); ?>
		
	</body>
</html>