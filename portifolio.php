<!doctype html>
<html lang="pt-br">
	<head>
		<?php require_once("header.html"); ?>
	</head>

	<body>
		<?php require_once("barranavegacao.html"); ?>
		

		<div class="container">
			<div class="row">
				<div class="page header">
					<h1>PRINCIPAIS SERVIÇOS EXECUTADOS</h1>
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12" style="text-align:center;margin-bottom:30px;">
					<img class="img-responsive rounded" src="img/SAGE.png">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-3 jumbotron" style="background:rgba(10,30,200,0.5);color:white;text-align:center;">
					<h2>2011</h2>
				</div>
				<div class="col-sm-6">
				<br/><br/><p> Implantação do COSGOIAS - Centro de Operação do Sistema da TER - Transenergia Renovável, utilizando o sistema SAGE. 
				</div>
			</div>	
			<div class="row" style="margin-bottom:20px;">
				<div class="col-sm-3 jumbotron" style="background:rgba(10,30,200,0.5);color:white;text-align:center;">
					<h2>2012</h2>
				</div>
				<div class="col-sm-6">
				<br/><p>  Parametrização do sistema SAGE - S/E PLANALTO (CELG GT)
				<p>Parametrização e implantação do sistema SAGE - S/E UCM
				<p>Parametrização do sistema SAGE - S/E AGUAS LINDAS (CELG GT) 
				</div>
				<div class="col-sm-3"></div>
			</div>
			<div class="row" style="margin-bottom:20px;">
				<div class="col-sm-3"></div>
				<div class="col-sm-3 jumbotron" style="background:rgba(10,30,200,0.5);color:white;text-align:center;">
					<h2>2012-2015</h2>
				</div>
				<div class="col-sm-6" >
				<br/><p>  Responsável técnico do COSGOIAS, tendo participado de todas as ampliações e das modificações de configuração do sistema de telecomunicações, envolvendo o COSGOIAS, COSR-NCO (Brasilia), COSR-S (Florianópolis) e subestações da TER.
				</div>
				
			</div>
			<div class="row" style="margin-bottom:20px;">
				
				<div class="col-sm-3 jumbotron" style="background:rgba(10,30,200,0.5);color:white;text-align:center;">
					<h2>2015</h2>
				</div>
				<div class="col-sm-9" >
				<br/><p>  Parametrização e implantação da ampliação do SAGE da S/E RIO BRANCO (EletroACRE)
				<p> Padronização da base de dados do SAGE da S/E CAMPO GRANDE II (Pantanal)
				<p>Parametrização da ampliação do SAGE da S/E ZONA OESTE (Furnas)
				<p> Parametrização da ampliação do SAGE da S/E EDEIA Transmissão (TER / VOTORATIM)
				</div>
				
			</div>	
		</div>

		<?php require_once("footer.html"); ?>
		
	</body>
</html>