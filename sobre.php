<!doctype html>
<html lang="pt-br">
	<head>
		<?php require_once("header.html"); ?>
	</head>

	<body>
		<?php require_once("barranavegacao.html"); ?>
		

		<div class="container">
			
			<div class="row">
				<div class="page-header">
					<h1>A EMPRESA</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5">
					A RT ELETRICIDADE E INFORMÁTICA foi criada no final dos anos 90, com o objetivo de prestar serviços na área de projetos elétricos industriais e automação de sistemas elétricos.
					<br/><br/>
					Após mais de 10 anos no mercado, se consolidou como uma empresa especializada na automação, tanto de subestações como centros de operação.
				</div>
				<div class="col-sm-2"></div>
				<div class="col-sm-5">
					<img class="img-responsive rounded" src="img/img-sobre.jpg">
				</div>
			</div>		
			<div class="row">
				<div class="page-header">
					<h1>ENGENHEIROS</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-2">
					<img class="img-responsive img-rounded" src="img/joseantonio.jpg"/>
				</div>
				<div class="col-sm-10 jumbotron">
				<h2>José Antônio Ribeiro Tannus</h2>
				<h6>Eng. Elétrico pela UFG</h6>
				<h6>Especialista em Eng. Elétrico pela Unifei</h6>
				<h6>Eng. Sênior na CELG DISTRIBUIÇÃO, onde trabalhou 36 anos.</h6><br/>
				<h4><STRONG>Projetos Anteriores à RT:</STRONG></h4>
				<HR>
				<h6>Gerente do Setor de Programação e Desligamento - 1986-1988</h6>
				<h6>Gerente do Centro de Operação do Sistema - 1988-1989</h6>
				<h6>Gerente do Setor de Engenharia de Automação da Operação - 1991-2012</h6>
				<h6>Gerente do Setor de Acompanhamento Energético - 2012-2013</h6>

				<h6>1991-1999 - Responsável pela concepção, desenvolvimento e implantação do primeiro sistema de supervisão e controle do COS da CELG </h6>
				<h6>1999-2012 - Responsável pela especificação, aquisição e implantação do sistema SAGE no COS da CELG </h6>
				<h6>1991-2012 - Mais de 100 subestações digitalizadas e/ou supervisionadas, com sistemas da EFACEC, ABB, SIEMENS, ELEBRA e STD.</h6>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-2">
					<img class="img-responsive img-rounded" src="img/alexandre.jpg"/>
				</div>
				<div class="col-sm-10 jumbotron">
				<h2>Alexandre Moraes Tannus</h2>
				<h6>Eng. da Computação pela PUC-GO</h6>
				<h6>Mestre em Eng. Elétrica pela UFMG</h6>
				<h6>Técnico em Telecomunicações pelo CEFET-GO (hoje IFG).
				</h6>
				</div>
			</div>

			<!--<div class="row">
				<div class="col-sm-2">
					<img class="img-responsive img-rounded" src="img/gabi.jpg"/>
				</div>
				<div class="col-sm-10 jumbotron">
				<h2>Gabrielle de Oliveira Perdigão</h2>
				<h6>Eng. de Controle e Automação pela UFMG</h6>
				<h6>Freelancer ocasional.
				</h6>
				</div>
			</div>-->
		</div>
		
		<?php require_once("footer.html"); ?>
		
	</body>
</html>